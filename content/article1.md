+++
title = "Meeting on the December 2nd"
date = 2021-12-04
+++
On December the 2nd we had a meeting @**Kung Fu Tea** - there we discussed a variety of topics peritent to the club (such as the gack black paper)

Some conclusions reached:
- We need a project; information freedom project would be a good thing to start on. 
- We'll go to Menards on Saturday to buy supplies
- Poster design - Aditya and Devin to recruit more members to the club
- The clubs purpose needs to be further solidified

Hopefully more people can attend future meetings. As always, remember you can keep up with club events by following our RSS feed!